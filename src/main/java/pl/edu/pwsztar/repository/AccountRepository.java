package pl.edu.pwsztar.repository;

import pl.edu.pwsztar.entity.Account;

import java.util.List;

public interface AccountRepository {
    Account getByNumber(int accountNumber);

    Account save(Account account);

    void delete(int accountNumber);

    List<Account> findAll();
}
