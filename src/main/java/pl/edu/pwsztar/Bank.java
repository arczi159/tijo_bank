package pl.edu.pwsztar;

import pl.edu.pwsztar.entity.Account;
import pl.edu.pwsztar.repository.AccountRepository;
import pl.edu.pwsztar.repository.AccountStorage;

import java.util.Optional;


class Bank implements BankOperation {

    private final int ZERO = 0;
    static int accountNumber = 0;
    private final AccountRepository accountRepository;

    public Bank() {
        this.accountRepository = new AccountStorage() {
        };
    }

    public int createAccount() {
        ++accountNumber;
        accountRepository.save(new Account(accountNumber, ZERO));

        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        final int deletedAccountBalance = accountBalance(accountNumber);
        if (deletedAccountBalance != ACCOUNT_NOT_EXISTS) {
            accountRepository.delete(accountNumber);
        }

        return deletedAccountBalance;
    }

    public boolean deposit(int accountNumber, int amount) {
        final Account account = getAccount(accountNumber);
        if (Optional.ofNullable(account).isPresent()) {
            account.deposit(amount);
            accountRepository.save(account);
            return true;
        }

        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        final Account account = getAccount(accountNumber);
        if (Optional.ofNullable(account).isPresent()) {
            final boolean wasWithdrawn = account.withdraw(amount);
            accountRepository.save(account);
            return wasWithdrawn;
        }

        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        Account account = getAccount(fromAccount);
        Account sndAccount = getAccount(toAccount);
        if (Optional.ofNullable(account).isPresent() && Optional.ofNullable(sndAccount).isPresent()) {
            return account.transfer(sndAccount, amount);
        }

        return false;
    }

    public int accountBalance(int accountNumber) {
        Account account = getAccount(accountNumber);

        return Optional.ofNullable(account)
                .map(Account::getAccountBalance)
                .orElse(ACCOUNT_NOT_EXISTS);
    }

    public int sumAccountsBalance() {
        return accountRepository.findAll()
                .stream()
                .mapToInt(Account::getAccountBalance)
                .sum();
    }

    Account getAccount(int accountNumber) {
        return accountRepository.getByNumber(accountNumber);
    }
}
