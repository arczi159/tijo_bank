package pl.edu.pwsztar.entity;

public class Account {
    private final int accountNumber;
    private int accountBalance;

    public Account(int accountNumber,
                   int accountBalance) {
        this.accountNumber = accountNumber;
        this.accountBalance = accountBalance;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getAccountBalance() {
        return accountBalance;
    }

    public void deposit(int amount) {
        this.accountBalance += amount;
    }

    public boolean withdraw(int amount) {
        if (this.accountBalance >= amount) {
            this.accountBalance -= amount;
            return true;
        }

        return false;
    }

    public boolean transfer(Account transferAccount, int amount) {
        if (this.accountBalance >= amount) {
            withdraw(amount);
            transferAccount.deposit(amount);
            return true;
        }
        return false;
    }
}
